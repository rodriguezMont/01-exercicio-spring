package br.com.itau.investimento.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.services.CatalogoService;
import br.com.itau.investimento.services.SimulacaoService;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

	@Autowired
	SimulacaoService simulacaoService;
	
	@Autowired
	CatalogoService catalogoService;
	
	@PostMapping
	public List<Simulacao> simular(@RequestBody Aplicacao aplicacao){
		Produto produto = catalogoService.obterProdutoPorId(aplicacao.getIdProduto());
		
		if(produto == null) {
			return null;
		}
		
		return simulacaoService.calcular(produto, aplicacao.getValor());
	}
}
